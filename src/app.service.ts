import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from './constants/jwt.constant';
import { AuthDto } from './dto/auth.dto';
import { Token } from './types/token.types';
import { catchError, firstValueFrom } from 'rxjs';

@Injectable()
export class AppService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly httpService: HttpService,
  ) {}

  async authUser(authDto: AuthDto): Promise<Token> {
    // TODO: Get JWT Payload from Azure AD
    const accessToken: string = authDto.access_token;
    const splitAccessToken: string[] = accessToken.split('.');
    const jwtHeader: any = JSON.parse(
      Buffer.from(splitAccessToken[0], 'base64').toString('ascii'),
    );
    const publicKey: string = await this.getPublicKey(jwtHeader?.kid);
    const payload = (await this.decodedValidToken(
      accessToken,
      publicKey,
    )) as any;
    console.log(payload);
    // Signed new token with our own secret
    const newToken = await this.getTokens(payload);
    return newToken;
  }

  async verifyUser(user: any): Promise<any> {
    return user;
  }

  // Utilities Function
  async getTokens(payload: any): Promise<Token> {
    const at = await this.jwtService.signAsync(
      {
        email: payload.upn,
      },
      {
        secret: jwtConstants.secret,
        expiresIn: 60, // 1 Minute
      },
    );
    return { access_token: at };
  }

  async getPublicKey(jwtHeaderKID: string): Promise<any> {
    const { data } = await firstValueFrom(
      this.httpService
        .get(
          `https://login.microsoftonline.com/${jwtConstants.tenantId}/discovery/keys?appid=${jwtConstants.clientId}`,
        )
        .pipe(
          catchError((error) => {
            console.error(error.response.data);
            throw 'An error happened!';
          }),
        ),
    );
    return data?.keys.find(({ kid }) => kid === jwtHeaderKID)?.x5c[0];
  }

  async decodedValidToken(accessToken: string, publicKey: string) {
    const key = `-----BEGIN CERTIFICATE-----\n${publicKey}\n-----END CERTIFICATE-----`;
    // decode & verify token
    return await this.jwtService.verify(accessToken, {
      publicKey: key,
    });
  }
}
