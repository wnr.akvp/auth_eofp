import { Controller, Post, HttpCode, HttpStatus, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { GetCurrentUser } from './decorator/get-current-user.decorator';
import { Public } from './decorator/public.decorator';
import { AuthDto } from './dto/auth.dto';
import { Token } from './types/token.types';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('auth')
  @Public()
  @HttpCode(HttpStatus.OK)
  authUser(@Body() authDto: AuthDto): Promise<Token> {
    return this.appService.authUser(authDto);
  }

  @Post('verify')
  @HttpCode(HttpStatus.OK)
  verifyUser(@GetCurrentUser() user: any): Promise<any> {
    return this.appService.verifyUser(user);
  }
}
